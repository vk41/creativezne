<?php 

class ContactForm {
    protected $requestData;
    protected $request;
    protected $httpCode = 400;
    protected $message = '';
    protected $responseData = [];
    protected $ourEmail = 'vishnukoothattu@gmail.com';


    public function __construct () {
        $this->request = $_SERVER['REQUEST_METHOD'];
        $inputJSON = file_get_contents('php://input');
        $this->requestData = json_decode($inputJSON, TRUE); //convert JSON into array
        
        
    }

    public function handle() {
        if(strtoupper($this->request) !== 'POST') {
            return $this->setResponse(405, 'Method Not Allowed');
        }

        $badFormData = $this->isBadForm();
        if($badFormData) {
            return $this->setResponse(400, 'Invalid Data', $badFormData);
        }
        
        // good form
        try {
            $this->sendMailToUs();
            $this->sendMailToUser();
            return $this->setResponse(200, 'Request received. We will contact you at the earliest.');
        } catch(Exception $r) {
            return $this->setResponse(500, 'Something went wrong on our side. We are working on fixing the issue.');
        }


    }

    protected function sendMail($to, $subject, $body, $headers = []) {
        if(!mail($to, $subject, $body, implode("\r\n", $headers))) {
            throw new Exception('Error sending mail.');
        }
    }

    protected function sendMailToUser() {
        $body = [];
        $body[] = 'Dear ' . ucwords($this->requestData['name']) . ' <br/><br/>';
        $body[] = '<p> Thank you for contacting us, our team will repond to your request at the ealiest.</p>';
        $body[] = '<p>Thanks and regards</p>';

        $this->sendMail($this->requestData['email'], 'Contact request confirmation.', implode(' ',$body));
    }


    protected function sendMailToUs() {
        $body = [];
        $body[] = 'Hello  <br/><br/>';
        $body[] = 'We have received a contact request from ' . ucwords($this->requestData['name']) . ' <br/><br/>';
        $body[] = '<table style="width:500px">';
        $body[] = '<tr><td>Name</td><td>' . ucwords($this->requestData['name']) . '</td> </tr>';
        $body[] = '<tr><td>Mobile/td><td>' . $this->requestData['mobile'] . '</td> </tr>';
        $body[] = '<tr><td>Email/td><td>' . $this->requestData['email'] . '</td> </tr>';
        $body[] = '</table>';
        $body[] = '<p>You can reply to this email.</p>';

        $headers = ['From: '.$this->requestData['email'],  'Reply-To: '.$this->requestData['email'],'X-Mailer: PHP/' . phpversion()];

        $this->sendMail($this->requestData['email'], 'Contact request confirmation.', implode(' ',$body), $headers);
    }
    

    protected function isBadForm() {
        $errorMessages = [];
        $errorFields = [];

        if(!isset($this->requestData['name']) ||  $this->requestData['name'] == '') {
            $errorMessages[] = 'Full name cannot be empty.';
            $errorFields [] = 'name';
        }
        
        if(!isset($this->requestData['mobile']) ||  $this->requestData['mobile'] == '') {
            $errorMessages[] = 'Mobile cannot be empty.';
            $errorFields [] = 'mobile';
        }

        if(!isset($this->requestData['email']) ||  $this->requestData['email'] == '') {
            $errorMessages[] = 'Email cannot be empty.';
            $errorFields [] = 'email';
            
        }
        if(isset($this->requestData['email']) && !filter_var($this->requestData['email'], FILTER_VALIDATE_EMAIL)) {
            $errorMessages[] = 'Email is invalid format.';
            $errorFields [] = 'email';
        }

        

        if(empty($errorFields)) {
            return false;
        }
        return ['fields' => $errorFields, 'messages' => $errorMessages];

    }

    protected function setResponse($code, $message, $data = []) {
        $this->httpCode = $code;
        $this->message = $message;
        $this->responseData = $data;
        return $this;
    }

    public function sendResponse() {
        http_response_code($this->httpCode);
        $this->response($this->message, $this->responseData);
    }

    protected function response($message = null, $data = []) {
        $response = [];
        if($message) {
            $response['message'] = $message;
        }

        if(!empty($data)) {
            $response['data'] = $data;
        }
        echo json_encode($response);
    }
}


(new ContactForm)->handle()->sendResponse();
