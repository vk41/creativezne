import './style.css'
import '@fortawesome/fontawesome-free/css/all.css'
import Swal from 'sweetalert2/dist/sweetalert2.js'

import 'sweetalert2/dist/sweetalert2.css'
import axios from 'axios';

window.Swal = Swal;

import Alpine from 'alpinejs'

window.Alpine = Alpine

Alpine.data('contactForm', () => ({
    formData: {
        name: "",
        mobile: "",
        email: ""
    },
    submiting: false,
    submit() {
        this.submiting = true;
        axios.post(this.$refs.contactform.action, this.formData)
            .then((response) => {
                this.clearForm();
                this.submiting = false;
                var alertMessage = {
                    title: response.data.message,
                    icon: 'success',
                    confirmButtonText: 'Ok'
                }
                Swal.fire(alertMessage);


            })
            .catch((err) => {
                this.submiting = false;
                var alertMessage = {
                    title: err.response.data.message,
                    icon: 'error',
                    confirmButtonText: 'Ok'
                }

                try {
                    alertMessage.html = err.response.data.data.messages.join('<br/>');
                } catch (er) {
                    console.log(er.message);
                }


                Swal.fire(alertMessage);
            })
    },
    clearForm() {
        this.formData.name = '';
        this.formData.mobile = '';
        this.formData.email = '';
    }
}));


Alpine.start()
// window.contactForm = 'sdf';
// window.contactForm = () => ({
//     name: '',
//     phone: '',
//     email: '',
//     submiting: false,

//     submit() {
//         this.submiting = true;
//     }
// })